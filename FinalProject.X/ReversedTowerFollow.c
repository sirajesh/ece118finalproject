/*
 * File: TemplateFSM.c
 * Author: J. Edward Carryer
 * Modified: Gabriel H Elkaim
 *
 * Template file to set up a Flat State Machine to work with the Events and Services
 * Frameword (ES_Framework) on the Uno32 for the CMPE-118/L class. Note that this file
 * will need to be modified to fit your exact needs, and most of the names will have
 * to be changed to match your code.
 *
 * This is provided as an example and a good place to start.
 *
 *Generally you will just be modifying the statenames and the run function
 *However make sure you do a find and replace to convert every instance of
 *  "Template" to your current state machine's name
 * History
 * When           Who     What/Why
 * -------------- ---     --------
 * 09/13/13 15:17 ghe      added tattletail functionality and recursive calls
 * 01/15/12 11:12 jec      revisions for Gen2 framework
 * 11/07/11 11:26 jec      made the queue static
 * 10/30/11 17:59 jec      fixed references to CurrentEvent in RunTemplateSM()
 * 10/23/11 18:20 jec      began conversion from SMTemplate.c (02/20/07 rev)
 */


/*******************************************************************************
 * MODULE #INCLUDE                                                             *
 ******************************************************************************/

#include "ES_Configure.h"
#include "ES_Framework.h"
#include "ReversedTowerFollow.h"

#include <BOARD.h>
#include "bot.h"
#include <stdio.h>


/*******************************************************************************
 * MODULE #DEFINES                                                             *
 ******************************************************************************/

#define TIMER_1_TICKS 750
/*******************************************************************************
 * PRIVATE FUNCTION PROTOTYPES                                                 *
 ******************************************************************************/
/* Prototypes for private functions for this machine. They should be functions
   relevant to the behavior of this state machine.*/


/*******************************************************************************
 * PRIVATE MODULE VARIABLES                                                            *
 ******************************************************************************/

/* You will need MyPriority and the state variable; you may need others as well.
 * The type of state variable should match that of enum in header file. */

typedef enum {
    InitPState,
    Search,
    FWD,
    BACKUP,
    BACKUP2,
    SRIGHT,
    SLEFT,
    SWEEP,
    WIGGLE,
    WIGGLE2,
    ALLIGN,
    FOLLOW,
    OPENUP,
    SWEEP2,
    OPENUP2,
    LINEUP,
    FOLLOW2,
    STOP,
} TemplateFSMState_t;

static const char *StateNames[] = {
	"InitPState",
	"Search",
	"FWD",
	"BACKUP",
	"BACKUP2",
	"SRIGHT",
	"SLEFT",
	"SWEEP",
	"WIGGLE",
	"WIGGLE2",
	"ALLIGN",
	"FOLLOW",
	"OPENUP",
	"SWEEP2",
	"OPENUP2",
	"LINEUP",
	"FOLLOW2",
	"STOP",
};


static TemplateFSMState_t CurrentState = InitPState; // <- change enum name to match ENUM
static uint8_t MyPriority;


/*******************************************************************************
 * PUBLIC FUNCTIONS                                                            *
 ******************************************************************************/

/**
 * @Function InitTemplateFSM(uint8_t Priority)
 * @param Priority - internal variable to track which event queue to use
 * @return TRUE or FALSE
 * @brief This will get called by the framework at the beginning of the code
 *        execution. It will post an ES_INIT event to the appropriate event
 *        queue, which will be handled inside RunTemplateFSM function. Remember
 *        to rename this to something appropriate.
 *        Returns TRUE if successful, FALSE otherwise
 * @author J. Edward Carryer, 2011.10.23 19:25 */
uint8_t InitReverseTowerFollowFSM(void) {
    ES_Event returnEvent;

    CurrentState = InitPState;
    returnEvent = RunReverseTowerFollowFSM(INIT_EVENT);
    if (returnEvent.EventType == ES_NO_EVENT) {
        return TRUE;
    }
    return FALSE;

}


/**
 * @Function RunTemplateFSM(ES_Event ThisEvent)
 * @param ThisEvent - the event (type and param) to be responded.
 * @return Event - return event (type and param), in general should be ES_NO_EVENT
 * @brief This function is where you implement the whole of the flat state machine,
 *        as this is called any time a new event is passed to the event queue. This
 *        function will be called recursively to implement the correct order for a
 *        state transition to be: exit current state -> enter next state using the
 *        ES_EXIT and ES_ENTRY events.
 * @note Remember to rename to something appropriate.
 *       Returns ES_NO_EVENT if the event have been "consumed."
 * @author J. Edward Carryer, 2011.10.23 19:25 */
static int TrackWireFound = 0;
static int TrackWireFound2 = 0;

ES_Event RunReverseTowerFollowFSM(ES_Event ThisEvent) {
    uint8_t makeTransition = FALSE; // use to flag transition
    TemplateFSMState_t nextState; // <- need to change enum type here

    ES_Tattle(); // trace call stack

    switch (CurrentState) {
        case InitPState: // If current state is initial Psedudo State
            if (ThisEvent.EventType == ES_INIT)// only respond to ES_Init
            {
                // this is where you would put any actions associated with the
                // transition from the initial pseudo-state into the actual
                // initial state
                // now put the machine into the actual initial state
                ES_Timer_InitTimer(1, 250);
                ES_Timer_InitTimer(6, 25);
                nextState = WIGGLE;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case BACKUP:
            Bot_RightMtrSpeed(-60);
            Bot_LeftMtrSpeed(-60);
            if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == 1) {
                ES_Timer_InitTimer(1, 400);
                nextState = SLEFT;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case BACKUP2:
            Bot_RightMtrSpeed(-60);
            Bot_LeftMtrSpeed(-60);
            if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == 1) {
                ES_Timer_InitTimer(1, 400);
                nextState = SRIGHT;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case SRIGHT:
            Bot_RightMtrSpeed(80);
            Bot_LeftMtrSpeed(-80);
            if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == 1) {
                ES_Timer_InitTimer(1, 1500);
                Bot_RightMtrSpeed(0);
                Bot_LeftMtrSpeed(0);
                nextState = SWEEP;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case SLEFT:
            Bot_RightMtrSpeed(100);
            Bot_LeftMtrSpeed(-100);
            if (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == 1) {
                ES_Timer_InitTimer(1, 2000);
                Bot_RightMtrSpeed(0);
                Bot_LeftMtrSpeed(0);
                nextState = SWEEP;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case SWEEP:
            Bot_RightMtrSpeed(0);
            Bot_LeftMtrSpeed(0);
            Bot_MidMtrSpeed(100);
            if (ThisEvent.EventType == RSIDE || ThisEvent.EventType == ES_TIMEOUT) {
                ES_Timer_InitTimer(1, 250);
                nextState = OPENUP;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case OPENUP:
            Bot_MidMtrSpeed(-100);
            if (ThisEvent.EventType == NB || ThisEvent.EventType == ES_TIMEOUT) {
                Bot_MidMtrSpeed(0);
                ES_Timer_InitTimer(1, 500);
                nextState = WIGGLE;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case WIGGLE:
            Bot_MidMtrSpeed(100);
            Bot_LeftMtrSpeed(72);
            Bot_RightMtrSpeed(0);
            if ((ThisEvent.EventType == FRB) || (ThisEvent.EventType == FLB) || (ThisEvent.EventType == FRONTB)) {
                ES_Timer_InitTimer(1, 400);
                nextState = BACKUP2;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            if (ThisEvent.EventType == SIDEF || ThisEvent.EventType == RSIDE || (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == 1) ) {
                ES_Timer_InitTimer(1, 500);
                nextState = ALLIGN;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            if ((ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == 6)) {
                ES_Timer_InitTimer(6, 25);
                if (Bot_ReadBumpers() == 32) {
                    ES_Timer_InitTimer(1, 500);
                    nextState = ALLIGN;
                    makeTransition = TRUE;
                    ThisEvent.EventType = ES_NO_EVENT;
                }
            }
            break;
        case ALLIGN:
            Bot_LeftMtrSpeed(-72);
            Bot_RightMtrSpeed(45);
            Bot_MidMtrSpeed(100);
            if ((ThisEvent.EventType == FRB) || (ThisEvent.EventType == FLB) || (ThisEvent.EventType == FRONTB)){
                ES_Timer_InitTimer(1, 400);
                nextState = BACKUP2;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            if (ThisEvent.EventType == SIDEB || (ThisEvent.EventType == ES_TIMEOUT && ThisEvent.EventParam == 1)) {
                ES_Timer_InitTimer(1, 150);
                nextState = FOLLOW;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case FOLLOW:
            Bot_RightMtrSpeed(69);
            Bot_LeftMtrSpeed(69);
            Bot_MidMtrSpeed(100);
            if ((ThisEvent.EventType == FRB) || (ThisEvent.EventType == FLB) || (ThisEvent.EventType == FRONTB)) {
                ES_Timer_InitTimer(1, 400);
                nextState = BACKUP2;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            if (ThisEvent.EventType == ES_TIMEOUT) {
                ES_Timer_InitTimer(1, 2000);
                ES_Timer_InitTimer(6, 25);
                nextState = WIGGLE;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case LINEUP:
            Bot_LeftMtrSpeed(55);
            Bot_RightMtrSpeed(0);
            if (ThisEvent.EventType == SIDEB || ThisEvent.EventType == ES_TIMEOUT) {
                nextState = FOLLOW2;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case FOLLOW2:
            Bot_LeftMtrSpeed(-55);
            Bot_RightMtrSpeed(-55);
            if (ThisEvent.EventType == L || ThisEvent.EventType == F) {
                nextState = STOP;
                makeTransition = TRUE;
                ThisEvent.EventType = ES_NO_EVENT;
            }
            break;
        case STOP:
            Bot_RightMtrSpeed(0);
            Bot_LeftMtrSpeed(0);
            Bot_MidMtrSpeed(0);
            Bot_PuncherMtrSpeed(100);
            break;
        default: // all unhandled states fall into here
            break;
    } // end switch on Current State
    if (makeTransition == TRUE) { // making a state transition, send EXIT and ENTRY
        // recursively call the current state with an exit event
        RunReverseTowerFollowFSM(EXIT_EVENT);
        CurrentState = nextState;
        RunReverseTowerFollowFSM(ENTRY_EVENT);
    }
    ES_Tail(); // trace call stack end
    return ThisEvent;
}


/*******************************************************************************
 * PRIVATE FUNCTIONS                                                           *
 ******************************************************************************/
